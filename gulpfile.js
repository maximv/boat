'use strict';

const { src, dest, series, parallel, watch } = require('gulp');
const less = require('gulp-less');
const scss = require('gulp-sass');
const gulpif = require('gulp-if');
const autoprefixer = require('gulp-autoprefixer');
const uglifycss = require('gulp-uglifycss');
const svgstore = require('gulp-svgstore');
const cheerio = require('gulp-cheerio');
const inject = require('gulp-inject');
const minify = require('gulp-minify');
const base64 = require('gulp-base64');

const isDev = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

function buildScss() {
    return src('./src/scss/styles.scss')
        .pipe(scss({
            includePaths: ['node_modules']
        }))
        .pipe(base64({
            baseDir: './src/images/base64',
            extensions: ['svg', 'png'],
        }))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(gulpif(!isDev, uglifycss()))
        .pipe(dest('./dist/css'));
}

function buildLess() {
    return src('./src/less/base.less')
        .pipe(less())
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(gulpif(!isDev, uglifycss()))
        .pipe(dest('./dist/css'));
}

function buildVendors() {
  return src([
    './node_modules/jquery/dist/jquery.min.js',
    './node_modules/jquery.maskedinput/src/jquery.maskedinput.js',
    './node_modules/owl.carousel2/dist/owl.carousel.min.js',
    './node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js',
    './node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
  ])
    .pipe(
      gulpif(
        '!**/*.min.js',
        minify({
          noSource: true,
          ext: { min: '.min.js' },
        })
      )
    )
    .pipe(dest('./dist/js/vendors/'));
}

function buildSvgIcons() {
    const svg = src('./src/images/icons/*.svg')
        .pipe(cheerio({
            run: function ($) {
                $('[fill]').removeAttr('fill');
            },
            parserOptions: {
                xmlMode: true
            }
        }))
        .pipe(svgstore({
            inlineSvg: true
        }));

    return src('./templates/icons.twig')
        .pipe(inject(svg, {
            starttag: '{# inject:icons #}',
            endtag: '{# endinject #}',
            transform: (filePath, file) => file.contents.toString()
        }))
        .pipe(dest('./templates'));
}

function watcher() {
    watch('./src/less/**/*.less', { usePolling: true }, buildLess);
    watch('./src/scss/**/*.scss', { usePolling: true }, buildScss);
}

exports.vendors = buildVendors;

exports.icons = buildSvgIcons;

exports.watch = series(parallel(buildLess, buildScss), watcher);

exports.build = parallel(buildLess, buildScss);