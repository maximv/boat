<?php

	require_once __DIR__ . '/vendor/autoload.php';

    $loader = new Twig_Loader_Filesystem(__DIR__ . '/templates');

    $twig = new Twig_Environment(
        $loader, array(
            'charset' => 'utf-8',
            //'cache' => DIR . '/cache/twig'
        )
    );

    $tpl = (!empty($_GET['path']) && $_GET['path'] != '/') ? $_GET['path'] : 'main';

	echo $twig->render(
        './' . $tpl . '.twig', array(
            'request_uri' => $_SERVER['REQUEST_URI']
        )
    );