$(function () {
  var product_thumbnails_lg = $('.owl-main');
  var product_thumbnails_sm = $('.owl-thumbs');

  product_thumbnails_lg.owlCarousel({
    items: 1,
    margin: 0,
    dots: false,
    loop: true,
  });

  product_thumbnails_sm.owlCarousel({
    responsive: {
      //Адаптивность. Кол-во выводимых элементов при определенной ширине.
      0: {
        items: 2,
      },
      768: {
        items: 5,
      },
    },
    margin: 4,
    dots: false,
    loop: true,
  });

  product_thumbnails_lg.on('changed.owl.carousel', function (event) {

    var elem_lg_index =
      $(event.target)
        .find('.owl-carousel-img')
        .eq(event.item.index)
        .attr('data-count');

    var elem_sm = product_thumbnails_sm
      .find('.owl-item.active')
      .has(`[data-count = ${elem_lg_index}]`)
      .eq(0);

    var elem_sm_index = elem_sm.index();

    product_thumbnails_sm.trigger('to.owl.carousel', [elem_sm_index + 1, 200, true]);

    $('.thumbnail').each(function () {
       $(this).removeClass('active');
    });

    $(elem_sm).find('.thumbnail').addClass('active');
  });

  product_thumbnails_sm.on('click', '.owl-item', function () {
    var self = $(this);
    var self_index =
        $(this)
        .find('.thumbnail')
        .attr('data-count');

    var lg_index = product_thumbnails_lg
      .find('.owl-item')
      .has(`[data-count = ${self_index}]`)
      .eq(0);

    product_thumbnails_lg.trigger('to.owl.carousel', [self_index, 200, true]);

    var thumbnail = self.find('.thumbnail');

     $('.thumbnail').each(function () {
       $(this).removeClass('active');
     });

    thumbnail.addClass('active');
  });
});
