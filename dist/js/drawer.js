(function (toggleButton, drawerLayer, drawerBodyLayer) {

  function toggleDrawer(action) {
    switch (action) {
      case 'show':
        return () => drawerLayer.classList.add('show');
      case 'hide':
        return () => drawerLayer.classList.remove('show');
    }
  }

  toggleButton.addEventListener('click', toggleDrawer('show'));
  drawerLayer.addEventListener('click', toggleDrawer('hide'));
  drawerBodyLayer.addEventListener('click', (e)=> e.stopPropagation());

})(document.querySelector(
  '.burger'
), document.querySelector(
  '.drawer'
), document.querySelector(
  '.drawer-body'
));

